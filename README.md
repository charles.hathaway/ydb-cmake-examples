# YDB Cmake Examples

Examples using the YDB Cmake support files to create plugins, MUMPS applications, and other things.

The goal is for the example to be concise and descriptive

## Table of Contents

### ydbcmake

This directory contains all the cmake files needed to compile these tools.
You should import it directly from the YDBCmake repository using:

```
git pull --allow-unrelated https://gitlab.com/YottaDB/Tools/YDBcmake.git
```

It can then by sync'ed by using the same command.

### ydbplugin

A template for a YottaDB plugin written in C using the cmake files
