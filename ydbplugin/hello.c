#include <stdio.h>
#include <libyottadb.h>

#include "helpers/helpers.h"

int main() {
  ydb_buffer_t *value;
  set("Hello world!", "^hello", 0);
  value = get("^hello", 0);
  value->buf_addr[value->len_used] = '\0';
  printf("Got value %s\n", value->buf_addr);

  free(value);
  return 0;
}
