/* Copyright (C) 2018-2019 YottaDB, LLC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef HELPERS_H
#define HELPERS_H

#include <stdio.h>
#include <stdarg.h>

#include <libyottadb.h>

#define YDB_ERROR_CHECK(status, z_status, msg) do {   \
	if(YDB_OK != status) {                              \
		YDB_LITERAL_TO_BUFFER("$ZSTATUS", (z_status));    \
		YDB_MALLOC_BUFFER((msg), 1024);                   \
		ydb_get_s(z_status, 0, NULL, (msg));              \
		(msg)->buf_addr[(msg)->len_used] = '\0';          \
		fprintf(stderr, "YDBError: %s:%d %s",             \
        __FILE__, __LINE__, (msg)->buf_addr);         \
		free((msg)->buf_addr);                            \
		(msg)->len_used = 0;                              \
		(msg)->len_alloc = 0;                             \
	}                                                   \
} while(0);

ydb_buffer_t *make_buffers(char *global, size_t num_args, va_list args);
void set(char *new_value, char *global, size_t num_args, ...);
ydb_buffer_t *get(char *global, size_t num_args, ...);

#endif
